# The Swedish Chef's Recipes CTF - Captain's Log
A demo project to have fun with Ruby, Hacking, RAG's, Security Logging (TINES), GCP, CI/CD...

# Languages & Frameworks Used on this project
- Ruby on Rails
- Docker
- Kubernetes (GKE)
- Google Cloud Platform
- Gitlab CI/CD
- TINES
- OpenAI
- Postgres
- Bootstrap
- CloudSQL
- Nginx

## Stardate 07.08.2024 - Setting Off
This is my first commit for this project. Fingers crossed. My plan as of now is to make a modern cooking website with a RAG system set up that is intentionally vulnerable to revealing sensitive info about the SQL database it's hooked up to. I want to hook up this intentionally vulnerable app to TINES so I can monitor for malicious queries and get alerts when someone is trying to be naughty. Let's see how it goes.

## Stardate 07.08.2024 - On the Rails
Second commit, I just got the skeleton of the website working and connecting to postgres. Users can sign up, login and chat with the "chatbot". Not doing much right now except echoing back input.

## Stardate 07.09.2024 - DockerCeption
Today I got my feet wet with setting up Gitlab's CI/CD and I ended up not deploying to firebase which I thought I would originally. I wanted to get some experience with using Google's Kubernetes Engine (GKE) so I created a new cluster and set up secrets and service accounts with that. I had trouble connecting my deployed instance to CloudSQL but eventually I got CloudSQLProxy working (I didn't have the CloudSQLAdmin API enabled in my project). I also thought it was cool to use dind (Docker in Docker) so that I could build and deploy my ruby on rails image within a docker image that the gitlab runners used.

## Stardate 07.10.2024 - Swedish Chef Time
Today was a good day, I finally got the LLM working the way I wanted it to and the CTF is almost done. I decided to go with a swedish chef theme for the site and the responses from OpenAI as the swedish chef really are the cherry on top.

## Stardate 07.10.2024 - hackmerecipes.com
Set up a new domain name with the deployed site which I thought was fitting and it was a great opportunity to learn about ingress and using nginx for load balancing and routing IP's in a GKE cluster
