import bcrypt
from tqdm import tqdm
# Function to compare a given bcrypt hashed string to all passwords in rockyou.txt
def compare_hash_to_rockyou(bcrypt_hash, rockyou_path):
    try:
        # Open the rockyou.txt file
        with open(rockyou_path, 'r', encoding='latin-1') as file:
            for password in tqdm(file):
                password = password.strip()
                # Check if the bcrypt hash matches the password
                if bcrypt.checkpw(password.encode('utf-8'), bcrypt_hash.encode('utf-8')):
                    print(f'Match found: {password}')
                    return
        print('No match found.')
    except FileNotFoundError:
        print(f'The file {rockyou_path} was not found.')
    except Exception as e:
        print(f'An error occurred: {e}')

# Main function to execute the comparison
if __name__ == '__main__':
    # Input bcrypt hash
    bcrypt_hash = input('Enter the bcrypt hashed string: ').strip()
    
    # Path to the rockyou.txt file
    rockyou_path = 'rockyou.txt'
    
    # Compare the bcrypt hash to the passwords in rockyou.txt
    compare_hash_to_rockyou(bcrypt_hash, rockyou_path)
