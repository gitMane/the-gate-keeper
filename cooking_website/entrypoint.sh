#!/bin/bash
set -e

# Ensure the database is up to date
bundle exec rails db:migrate 2>/dev/null || bundle exec rails db:setup

#Set up the seed
#bundle exec rails db:seed

# Execute the container's main process
exec "$@"
