Rails.application.routes.draw do
  devise_for :users
  root to: 'pages#home'
  get 'chat', to: 'pages#chat'
  get 'search_recipes', to: 'pages#search_recipes'
  get 'about', to: 'static#about'
end