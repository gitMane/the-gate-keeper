# app/models/recipe.rb
class Recipe < ApplicationRecord
    validates :name, presence: true
    validates :prepare_time, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
    validates :cook_time, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
    validates :ingredients, presence: true
    validates :instructions, presence: true
  end
  