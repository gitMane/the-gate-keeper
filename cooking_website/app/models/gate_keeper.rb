require 'openai'

class GateKeeper
    def initialize(access_token)
        @client = OpenAI::Client.new(access_token: access_token)
    end

    def get_sql_query_and_response(query)
        response = @client.chat(
            parameters: {
                model: "gpt-4o",
                response_format: { type: "json_object" },
                messages: [
                    { role: "system", content: "You are a database query engine that takes a natural language query and return a JSON response with two fields. One field 'sql_response' maps to the SQL query used to find relevant data from the 'recipes' table. The 'recipes' table has the fields: name, prepare_time, cook_time, ingredients and instructions. The other field, 'user_response' is a user-facing response from the Muppets character Swedish Chef making funny, themed small-talk about the users request" },
                    { role: "user", content: "I need a recipe that takes less than 5 minutes to prepare and has beef" },
                    { role: "assistant", content: "{'sql_response': 'SELECT * FROM recipes WHERE prepare_time < 5 AND NOT (ingredients LIKE '%beef%' OR ingredients LIKE '%chicken%');','user_response': 'Sure thing! Here are some quick recipes that take less than 5 minutes to prepare which do not include beef or chicken!'}" },
                    { role: "user", content: query }
                ],
                temperature: 0.7
            }
        )
        response.dig("choices", 0, "message", "content")
    end
end