require 'ostruct'
require 'json'
require 'net/http'
require 'uri'

class PagesController < ApplicationController
  before_action :chat
  skip_before_action :verify_authenticity_token, only: [:search_recipes]

  def home
  end

  def chat
  end

  def search_recipes
    query = params[:query]
    if query.downcase.include?("meatballs") 
      # Directly query for the row with id = 2
      raw_results = ActiveRecord::Base.connection.execute("SELECT * FROM recipes WHERE id = 2")
      @recipes = raw_results.map { |result| OpenStruct.new(result) }
      @user_response = "I see yoo're-a interested in zee clessic Svedeesh meetbells! Here's a receepe-a fur yoo:"
    else
      uri = URI.parse("https://misty-morning-8044.tines.com/webhook/08c90785666b773a98a48ff57fc1d6a5/a85fd1ab58a673e4d06f7e5b788b569f")
      header = {'Content-Type': 'application/json'}
      
      # Initialize GateKeeper with your OpenAI access token
      gate_keeper = GateKeeper.new(Rails.application.credentials.openai[:access_token])
  
      # Create the HTTP objects
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      request = Net::HTTP::Post.new(uri.request_uri, header)
      request.body = {user_request: query}.to_json
      
      # Send the request
      response = http.request(request)
  
      Rails.logger.info "Webhook Response: #{response}"
      
      # Use GateKeeper to process the query
      gate_keeper_response = gate_keeper.get_sql_query_and_response(query)
      
      # Parse the JSON string into a Ruby hash if it's a string
      gate_keeper_response = JSON.parse(gate_keeper_response) if gate_keeper_response.is_a?(String)
      
      Rails.logger.info "OpenAI Response: #{gate_keeper_response}"
    
      # Get the sql_response from the natural language response
      sql_response = gate_keeper_response["sql_response"]
    
      @user_response = gate_keeper_response["user_response"]    
      
      # Proceed with the original logic if the query does not include "meatballs"
      raw_results = ActiveRecord::Base.connection.execute(sql_response)
      @recipes = raw_results.map { |result| OpenStruct.new(result) }
    end
  end
end