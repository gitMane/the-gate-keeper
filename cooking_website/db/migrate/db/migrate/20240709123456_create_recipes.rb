class CreateRecipes < ActiveRecord::Migration[6.0]
    def change
      create_table :recipes do |t|
        t.string :name
        t.integer :prepare_time
        t.integer :cook_time
        t.text :ingredients
        t.text :instructions
  
        t.timestamps
      end
    end
  end
  